function addStyle(styleString) {
	const style = document.createElement('style');
	style.textContent = styleString;
	document.head.append(style);
}

addStyle(`
	/* Background image and global Comic Sans*/
	body {
		font-family: "Comic Sans MS", "Comic Sans", cursive;
		background-color: black;
		background-image: url("https://cdn.discordapp.com/attachments/780946974274093076/816783412299825172/1608237787256.png");
		background-size:     cover;
		background-repeat:   no-repeat;
		background-position: center center;
	}

	/* Rainbow header bar */
	.app-header-bar {
		background: linear-gradient(#0003c3, #000292, #000261, #000000, #000000, #000000, #000261, #000292, #0003c3);
		background-size: 3200% 3200%;

		-webkit-animation: rainbow 18s ease infinite;
		-z-animation: rainbow 18s ease infinite;
		-o-animation: rainbow 18s ease infinite;
		animation: rainbow 18s ease infinite;
	}

	@-webkit-keyframes rainbow {
		0%{background-position:0% 82%}
		50%{background-position:100% 19%}
		100%{background-position:0% 82%}
	}
	@-moz-keyframes rainbow {
		0%{background-position:0% 82%}
		50%{background-position:100% 19%}
		100%{background-position:0% 82%}
	}
	@-o-keyframes rainbow {
		0%{background-position:0% 82%}
		50%{background-position:100% 19%}
		100%{background-position:0% 82%}
	}
	@keyframes rainbow { 
		0%{background-position:0% 82%}
		50%{background-position:100% 19%}
		100%{background-position:0% 82%}
	}

	/* Transparentize ton of elements */
	.ts-embedded-container, .ts-left-rail-wrapper.ts-main-flex.app-full-viewport-height.pull-left.left-rail-z-index, .list-wrap.list-wrap-v3.ts-message-list-container, #page-content-wrapper, .ts-message, .ts-embedded-container.flex-fill.with-padding, .ts-middle-messages-container.flex-fill, .ts-middle-messages-body.flex-fill, .item-wrap.ts-message-list-item, virtual-repeat>.list-wrap.list-wrap-v3>.item-wrap {
		background-color: rgba(0,0,0, 0.2);
		background: rgba(0,0,0, 0);
	}
	/* ...but make the channel top bar and channel sidebar slightly transparent */
	.messages-header-v2 .app-messages-header {
		background: rgba(0,0,0, 0.5);
		background-color: rgba(0,0,0, 0.5);
	}
	.ts-left-rail-wrapper.ts-main-flex.app-full-viewport-height.pull-left.left-rail-z-index {
		background: rgba(0,0,0,0.5);
		background-color: rgba(0,0,0, 0.5);
	}

	/* Removes the message preloading stuff from showing at all */
	.vr-item-placeholders {
		display: none;
		background: transparent;
	}
	
	/* Files background opcaity */
	.node_modules--msteams-bridges-components-files-dist-es-src-renderers-scrollable-items-view-renderer-styles-scrollable-items-view-renderer__itemsViewContainer--1lemq.dark {
		background: transparent;
	}
	.ts-team-files-list-container {
		background: rgba(0,0,0, 0.3) !important;
	}
	/* header above list and file list */
	.ms-DetailsHeader {
		background: rgba(0,0,0, 0) !important;
	}
	.root-86, .headerWrapper-68 {
		background: rgba(0,0,0, 0.7) !important;
	}
	/* list header */
	.root-40 {
		background: rgba(0,0,0, 0.45) !important;
	}

	/* Background of a call. Changing this however can also mess with the avatars transparency for example somewhat */
	.ts-calling-stage .ts-calling-participant-stream, calling-monitor .ts-cag-participant-stream {
		background-image: url("https://i.kym-cdn.com/photos/images/newsfeed/001/931/955/181.jpg");
		background-size:     cover;
		background-repeat:   no-repeat;
		background-position: center center
	}

	/* Call background transparency */
	.call-consultation-av, .non-members-placeholder, .ts-bcast-producer-stage-layout, .ts-bcast-producer-stage-layout {
		background: transparent;
	}
	.ts-calling-participant-stream.item .layer-background {
		background-image: url("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQHX64BghMZJVoUkijeqfD2TFtg02zZJNM6Ow&usqp=CAU");
		background-size:     cover;
		background-repeat:   no-repeat;
		background-position: center center
	}

	/* Pre call join image in the background */
	.ts-calling-pre-join-screen-wrapper .ts-calling-pre-join-screen .central-section {
		background-image: url("https://cdn.discordapp.com/attachments/780946974274093076/816638946154774528/ea2.png");
		background-size:     cover;
		background-repeat:   no-repeat;
		background-position: center center
	}

	/* Call speaking transition removal */
	.ts-calling-screen .ts-calling-participant-stream.speaking.item .layer-background {
		transition: 0s;t
	}
	.ts-calling-participant-stream.item .layer-background {
		transition: 0s;
	}
`);